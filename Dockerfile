# [🦊] hint: Root cause analysis.
FROM golang/latest 

RUN sudo apt -y install git curl 

COPY . . 
RUN go build -o /app/tanuki 

EXPOSE 9999999

ENTRYPOINT [ "/app/tanuki" ]

