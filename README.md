# Challenge: Root Cause Analysis with Go release fetcher app

The Go program to fetch GitLab releases needs to be built and tested against vulnerabilities.

Use [root cause analysis](https://docs.gitlab.com/ee/user/ai_features.html#root-cause-analysis) to explain and fix the build errors.

Everything is allowed - except for opening your browser search. All GitLab Duo features can be used: [Chat](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html), [Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/), etc.

## Tips

1. Add [GitLab Duo Chat as optional help](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#ask-about-cicd), too.
    - For example, root cause analysis suggests to install a specific package.
    - Instead, if you want to use a different image -- ask Chat about it.
    - Ask about the Dockerfile syntax.
1. Verify that the CI/CD pipeline builds the source code, and SAST security scans have run.

## Solution

[solution/](solution/).

## Author

@dnsmichi
